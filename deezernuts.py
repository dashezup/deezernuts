import asyncio, hashlib, json, os
from aiohttp import web, ClientSession
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

app = web.Application()
routes = web.RouteTableDef()

magic = b"\xa4"

downloaderpage = open("downloader.html", "r").read()

def _md5(data):
    h = hashlib.md5()
    h.update(data.encode() if isinstance(data, str) else data)
    return h.hexdigest()

@routes.get("/geturl")
async def get_url(req):
    id = req.query.get("id")
    format = req.query.get("format")

    if not id:
        return web.Response(text="Please provide track ID")

    if not format:
        return web.Response(text="Please provide file format")

    if format == "flac":
        format = 9
    elif format == "mp3hq":
        format = 3
    elif format == "mp3":
        format = 1
    else:
        return web.Response(text="Invalid format")

    try:
        async with session.get("https://tv.deezer.com/smarttv/8caf9315c1740316053348a24d25afc7/app_access_token.php?device=lg") as res:
            token = json.loads(await res.text())["access_token"]
    except:
        return web.Response(text="Cant get access token")

    try:
        async with session.get(f"https://api.deezer.com/track/{id}?access_token={token}") as res:
            track = await res.json()
    except:
        return web.Response(text="Cant get track data")

    try:
        s1 = magic.join([track["md5_origin"].encode(), str(format).encode(), str(id).encode(), str(track["media_version"]).encode()])
        s2 = _md5(s1).encode() + magic + s1 + magic
        url = "https://e-cdns-proxy-" + track["md5_origin"][0] + ".dzcdn.net/api/1/" + Cipher(algorithms.AES(b"jo6aey6haid2Teih"), modes.ECB(), backend=default_backend()).encryptor().update(s2 + (b"." * (16 - (len(s2) % 16)))).hex()
    except:
        return web.Response(text="An error occurred while trying to get URL")

    return web.Response(text=url)

@routes.get("/getfile")
async def get_url(req):
    id = req.query.get("id")
    format = req.query.get("format")

    if not id:
        return web.Response(text="Please provide track ID")

    if not format:
        return web.Response(text="Please provide file format")

    if format == "flac":
        format = 9
        formatext = "flac"
    elif format == "mp3hq":
        format = 3
        formatext = "mp3"
    elif format == "mp3":
        format = 1
        formatext = "mp3"
    else:
        return web.Response(text="Invalid format")

    try:
        async with session.get("https://tv.deezer.com/smarttv/8caf9315c1740316053348a24d25afc7/app_access_token.php?device=lg") as res:
            token = json.loads(await res.text())["access_token"]
    except:
        return web.Response(text="Cant get access token")

    try:
        async with session.get(f"https://api.deezer.com/track/{id}?access_token={token}") as res:
            track = await res.json()
    except:
        return web.Response(text="Cant get track data")

    try:
        s1 = magic.join([track["md5_origin"].encode(), str(format).encode(), str(id).encode(), str(track["media_version"]).encode()])
        s2 = _md5(s1).encode() + magic + s1 + magic
        url = "https://e-cdns-proxy-" + track["md5_origin"][0] + ".dzcdn.net/api/1/" + Cipher(algorithms.AES(b"jo6aey6haid2Teih"), modes.ECB(), backend=default_backend()).encryptor().update(s2 + (b"." * (16 - (len(s2) % 16)))).hex()
    except:
        return web.Response(text="An error occurred while trying to get URL")

    try:
        async with session.get(url) as res:
            response = web.StreamResponse(
                headers={
                    "Content-Disposition": "attachment; filename=\"{} - {}.{}\"".format(track["artist"]["name"], track["title"], formatext),
                    "Content-Length": res.headers["Content-Length"]
                }
            )
            await response.prepare(req)

            async for chunk in res.content.iter_any():
                await response.write(chunk)
    except:
        await response.write("An error occurred while trying to stream data")

    await response.write_eof()
    return response

@routes.get("/")
async def downloader(req):
    return web.Response(text=downloaderpage, content_type="text/html")

async def s(): return ClientSession()
session = asyncio.get_event_loop().run_until_complete(s())

app.add_routes(routes)
web.run_app(app, port=os.environ.get("PORT", 8080))
