# Deezernuts
Deezer CDN URL generator and file downloader API

# API endpoints(HTTP GET)
`/geturl` -  Get Deezer CDN URL  
`/getfile` - Get file from Deezer CDN

# API parameters
`id` - Deezer track ID(can be acquired through Deezer search API)  
`format` - File format(see `Formats`)

# Formats
`flac` - Lossless audio(FLAC)  
`mp3hq` - MP3 320kbps  
`mp3` - MP3 128kbps

# Credits
Deemix: https://git.rip/RemixDev/deemix
